<?php

use \Orchestra\Testbench\TestCase;

class TestSamlAuthGuard extends TestCase{

	protected function getPackageProviders()
	{
		return array(
			'Sait\SamlAuth\SamlAuthServiceProvider'
		);
	}

	public function testRequireSamlAuth()
	{
		$stub = $this->getMock('SimpleSAML_Auth_Simple',array('requireAuth'));
		$stub->expects($this->once())
			->method('requireAuth')
			->with(array());

		$this->app['SimpleSAML_Auth_Simple'] = $stub;

		SamlAuth::requireSamlAuth();
	}

	public function testGetSamlKey()
	{
		$stub = $this->getMock('SimpleSAML_Auth_Simple',array('requireAuth','getAttributes'));

		Config::set('samlauth::samlResponseUserValidationField','testField');

		$samlResponse = array(
			'testField' => array('testValue')
		);

		$stub->expects($this->once())
			->method('getAttributes')
			->willReturn($samlResponse);

		$this->app['SimpleSAML_Auth_Simple'] = $stub;

		$this->assertEquals('testValue',SamlAuth::getSamlKey());
	}

	public function testGetAttributes()
	{
		$stub = $this->getMock('SimpleSAML_Auth_Simple',array('requireAuth','getAttributes'));

		Config::set('samlauth::samlResponseUserValidationField','testField');

		$samlResponse = array(
			'attribute 1' => array('testValue'),
			'attribute 2' => array('testValue'),
			'attribute 3' => array('testValue','testValue2')
		);

		$stub->expects($this->once())
			->method('getAttributes')
			->willReturn($samlResponse);

		$this->app['SimpleSAML_Auth_Simple'] = $stub;

		$attributes = SamlAuth::getAttributes();
		$this->assertArrayHasKey('attribute 1',$attributes);
		$this->assertEquals($attributes['attribute 1'],'testValue');
		$this->assertArrayHasKey('attribute 3',$attributes);
		$this->assertEquals($attributes['attribute 3'],array('testValue','testValue2'));
	}

	public function testGetSamlCredentials()
	{
		Config::set('samlauth::samlResponseUserValidationField','testField');
		Config::set('samlauth::userModelValidationField','username');

		$samlResponse = array(
			'testField' => array('testValue')
		);

		$stub = $this->getMock('SimpleSAML_Auth_Simple',array('requireAuth','getAttributes'));
		$stub->method('getAttributes')
			->willReturn($samlResponse);

		$this->app['SimpleSAML_Auth_Simple'] = $stub;

		$credentials = SamlAuth::getSamlCredentials();

		$this->assertArrayHasKey('username',$credentials);

		$this->assertEquals('testValue',$credentials['username']);

	}

	public function testLogOut()
	{
		$stub = $this->getMock('SimpleSAML_Auth_Simple',array('logout'));
		$stub->expects($this->once())
			->method('logout')
			->with('http://logoutRedirect.com');

		$this->app['SimpleSAML_Auth_Simple'] = $stub;

		SamlAuth::logout('http://logoutRedirect.com');
	}


}