<?php
use \Orchestra\Testbench\TestCase;
use \Sait\SamlAuth\User;

class TestSamlEloquentUserProvider extends TestCase{

	protected function getEnvironmentSetUp($app)
	{
		// reset base path to point to our package's src directory
		$app['path.base'] = __DIR__;

		$app['config']->set('database.default', 'testbench');
		$app['config']->set('auth.driver','saml');
		$app['config']->set('auth.model','\Sait\SamlAuth\User');
		$app['config']->set('database.connections.testbench', array(
			'driver'   => 'sqlite',
			'database' => ':memory:',
			'prefix'   => '',
		));


	}

	public function setUp()
	{
		parent::setUp();
		$artisan = $this->app->make('artisan');

		$artisan->call('migrate', array(
			'--database' => 'testbench',
			'--path'     => '../src/migrations',
		));

		$user = new User;
		$user->samlName = 'testUser';
		$user->email = 'saprogrammer@umontana.edu';
		$user->save();
	}


	protected function getPackageProviders()
	{
		return array(
			'Sait\SamlAuth\SamlAuthServiceProvider'
		);
	}

	public function testUserModelWorks()
	{
		$this->assertEquals('testUser',User::first()->samlName);
	}

	public function testUserAuthentication()
	{
		$samlResponse = array(
			'testField' => array('testValue')
		);

		Config::set('samlauth::userModelValidationField','samlName');

		$stub = $this->getMock('SimpleSAML_Auth_Simple',array('requireAuth','getAttributes'));
		$this->app['SimpleSAML_Auth_Simple'] = $stub;

		$this->assertTrue(Auth::attempt(array('samlName'=>'testUser')));

		$this->assertFalse(Auth::attempt(array('samlName'=>'testNotUser')));
	}


}