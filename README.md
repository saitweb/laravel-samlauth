# Laravel simpleSAMLphp #

This Laravel package handles authentication using simpleSAMLphp for Laravel 4.1

### This package provides the following ###

 * Loads simpleSAMLphp
 * SamlAuth helper
 * Eloquent User Model
 * SAML Eloquent Auth driver
 * Migration for creating simple user table

### Install ###
Add the repository to your composer.json and run composer update

You need to have configured SSH keys for bitbucket for the account that runs composer update in order to access this repository.

  	"repositories": [
	  {
		"type": "git",
		"url": "git@bitbucket.org:saitweb/laravel-samlauth.git"
	  }
	]


Register the package service provider in app/config/app.php

    'providers' => array(
                .....
                'Sait\SamlAuth\SamlAuthServiceProvider'
      );

### Configuration ###

There are four configuration options.
 
 * The simpleSAMLphp install location (simpleSamlPhpLocation)
 * The simpleSAMLphp service provider configuration name (serviceProviderConfigName)
 * Field the user model compares with for authorization (userModelValidationField)
 * Field of the SAML response uses to compare with the user model (samlResponseUserValidationField)
