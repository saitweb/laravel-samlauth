<?php namespace Sait\SamlAuth;

use \SimpleSAML_Auth_Simple;


class SamlAuthGuard
{
	protected $sa;

	public function __construct(SimpleSAML_Auth_Simple $sa)
	{
		$this->sa = $sa;
	}

	public function getAttribute($key,$options = array())
	{
		$this->requireSamlAuth($options);

		$samlAttribs = $this->sa->getAttributes();
		$samlUserArray = array_key_exists($key,$samlAttribs)?$samlAttribs[$key]:false;
		if($samlUserArray !== false)
		{
			return array_key_exists(0,$samlUserArray)?$samlUserArray[0]:false;
		}
		else
		{
			throw new Exception('SAML Response did not contain the requested key.');
		}
	}

	public function getSamlKey($options = array())
	{
		$key = \Config::get('samlauth::samlResponseUserValidationField');
		return $this->getAttribute($key,$options = array());
	}

	public function getAttributes()
	{
		$output = array();
		$samlAttribs = $this->sa->getAttributes();
		foreach($samlAttribs as $key=>$attrib){
			if(count($attrib) === 1){
				$output[$key] = $attrib[0];
			} else {
				$output[$key] = $attrib;
			}
		}
		return $output;
	}

	public function getSamlCredentials()
	{
		$samlEloquentUserKeyField = \Config::get('samlauth::userModelValidationField');
		return array($samlEloquentUserKeyField => $this->getAttribute(\Config::get('samlauth::samlResponseUserValidationField')));
	}

	public function requireSamlAuth($options = array())
	{
		$this->sa->requireAuth($options);
	}

	/**
	 * Logout
	 *
	 * This will logout of SAML Authentication
	 * and redirect to provided URL
	 *
	 * @param $url
	 */
	public function logout($url = null)
	{
		$this->sa->logout($url);
	}

	public function isAuthenticated()
	{
		return $this->sa->isAuthenticated();
	}
}