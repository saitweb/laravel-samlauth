<?php namespace Sait\SamlAuth;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Auth\UserInterface;

class SamlEloquentUserProvider extends EloquentUserProvider{

	private $simpleSAML;

	function __construct(\SimpleSAML_Auth_Simple $simpleSAML,$hash, $model)
	{
		parent::__construct($hash,$model);
		$this->simpleSAML = $simpleSAML;

	}

	public function retrieveByCredentials(array $credentials)
	{
		$query = $this->createModel()->newQuery();

		foreach ($credentials as $key => $value)
		{
			if ( ! str_contains($key, 'password')) $query->where($key, $value);
		}
		return $query->first();
	}

	/**
	 * Validate a user against the given credentials.
	 *
	 * @param  \Illuminate\Auth\UserInterface $user
	 * @param  array $credentials
	 * @return bool
	 */
	public function validateCredentials(UserInterface $user, array $credentials)
	{
		$samlEloquentUserKeyField = \Config::get('samlauth::userModelValidationField');
		return ($user->$samlEloquentUserKeyField === $credentials[$samlEloquentUserKeyField]);
	}

}