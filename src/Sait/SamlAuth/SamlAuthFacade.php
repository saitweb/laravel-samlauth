<?php
/**
 * Created by PhpStorm.
 * User: eric.seyden
 * Date: 10/30/14
 * Time: 2:53 PM
 */

namespace Sait\SamlAuth;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Illuminate\Auth\AuthManager
 * @see \Illuminate\Auth\Guard
 */
class SamlAuthFacade extends Facade {

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() { return 'SamlAuthGuard'; }

}