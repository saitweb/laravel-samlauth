<?php namespace Sait\SamlAuth;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class SamlAuthServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind('SimpleSAML_Auth_Simple',function($app){
			$simpleSAMLphpLocation = $app['config']['samlauth::simpleSamlPhpLocation'].'/lib/_autoload.php';
			$serviceProviderConfigName = $app['config']['samlauth::serviceProviderConfigName'];
			if(file_exists($simpleSAMLphpLocation))
			{
				include_once($simpleSAMLphpLocation);
				return new \SimpleSAML_Auth_Simple($serviceProviderConfigName);
			} else {
				throw(new \Exception('Could Not Find SimpleSAMLphp at '. $simpleSAMLphpLocation));
			}
		});

		$this->app->bind('Sait\SamlAuth\SamlAuthGuard',function($app){
			$sa = $app['SimpleSAML_Auth_Simple'];
			return new SamlAuthGuard($sa);
		});

		$this->app->bind('SamlAuthGuard','Sait\SamlAuth\SamlAuthGuard');
	}

	public function boot()
	{
		$this->package('sait/samlauth');

		AliasLoader::getInstance()->alias('SamlAuth', 'Sait\SamlAuth\SamlAuthFacade');

		$this->app['auth']->extend('saml',function($app){
			$model = $app['config']['auth.model'];
			$driver = $app['SimpleSAML_Auth_Simple'];
			$hash = $app['hash'];
			return new SamlEloquentUserProvider($driver,$hash,$model);
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
