<?php
return array(
	/*
	|--------------------------------------------------------------------------
	| Simple SAML PHP Location
	|--------------------------------------------------------------------------
	|
	| Simple SAML must be installed and configured as a service provider
	| on the server hosting this application.
	|
	*/
	'simpleSamlPhpLocation' => '/var/www/login',
	'serviceProviderConfigName' => 'default-sp',
	'userModelValidationField' => 'samlName',
	'samlResponseUserValidationField' => 'urn:oid:1.3.6.1.4.1.5923.1.1.1.6'
);